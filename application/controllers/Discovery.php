<?php

/**
 * Discovery controller.
 *
 * @category   framework
 * @package    discovery
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/discovery/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Discovery controller.
 *
 * @category   framework
 * @package    discovery
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/discovery/
 */

class Discovery extends ClearOS_REST_Controller
{
    public function __construct()
    {
        parent::__construct(FALSE);
    }

    function index()
    {
        $this->index_get();
    }

    function index_get()
    {
        // URLs that will show discovery information
        $discovery_uris = [ '/', '/discovery', '/discovery/', '/api', '/api/', '/api/v1', '/api/v1/' ];

        if (isset($_SERVER['REQUEST_URI']) && in_array($_SERVER['REQUEST_URI'], $discovery_uris)) {
            if ($this->authenticated && $this->authorized) {
                // TODO: add capabilities handler
                $data['capabilities'] = [];
            } else {
                $data['protip'] = 'Please set API credentials to see capabilities.';
            }

            return $this->respond_success($data);
        } else if ($this->authenticated && $this->authorized) {
            // Only respond with a 404 if allowed - we don't want to give away what's installed on this system.
            return $this->respond_not_found('Endpoint not found');
        } else {
            return $this->respond_unauthorized();
        }
    }
}
