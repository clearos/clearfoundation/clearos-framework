<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/** load the CI class for Modular Extensions **/
require dirname(__FILE__).'/Base.php';

/**
 * Modular Extensions - HMVC
 *
 * Adapted from the CodeIgniter Core Classes
 * @link	http://codeigniter.com
 *
 * Description:
 * This library replaces the CodeIgniter Controller class
 * and adds features allowing use of modules and the HMVC design pattern.
 *
 * Install this file as application/third_party/MX/Controller.php
 *
 * @copyright	Copyright (c) 2015 Wiredesignz
 * @version 	5.5
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 **/

/**
 * A common base class for all OS Controllers.
 */

class OS_Controller 
{
	public $autoload = array();
	
	public function __construct() 
	{
		$class = str_replace(CI::$APP->config->item('controller_suffix'), '', get_class($this));
		log_message('debug', $class." OS_Controller Initialized");
		Modules::$registry[strtolower($class)] = $this;	
		
		/* copy a loader instance and initialize */
		$this->load = clone load_class('Loader');
		$this->load->initialize($this);	
		
		/* autoload module items */
		$this->load->_autoloader($this->autoload);
	}
	
	public function __get($class) 
	{
		return CI::$APP->$class;
	}

    /* Add default methods */

    public function dashboard() {
        $this->load->page->view_dashboard_widget(uri_string());
    }

    public function help() {
        $this->load->page->view_help_widget(uri_string());
    }

    public function summary() {
        $this->load->page->view_summary_widget(uri_string());
    }
}

/**
 * A common base class for all OS REST Controllers.
 */

class OS_REST_Controller extends REST_Controller
{
    const HTTP_CODE_OK = 200;
    const HTTP_CODE_BAD_REQUEST = 400;
    const HTTP_CODE_UNAUTHORIZED = 401;
    const HTTP_CODE_FORBIDDEN = 403;
    const HTTP_CODE_NOT_FOUND = 404;
    const HTTP_CODE_METHOD_NOT_ALLOWED = 405;
    const HTTP_CODE_ALREADY_EXISTS = 409;
    const HTTP_CODE_VALIDATION_ERROR = 422;
    const HTTP_CODE_TOO_MANY_REQUESTS = 429;
    const HTTP_CODE_SERVER_ERROR = 500;

    const CODE_OK = '0';
    const CODE_BAD_REQUEST = '400.000';
    const CODE_UNAUTHORIZED = '401.000';
    const CODE_FORBIDDEN = '403.000';
    const CODE_NOT_FOUND = '404.000';
    const CODE_ALREADY_EXISTS = '409.000';
    const CODE_VALIDATION_ERROR = '422.000';
    const CODE_TOO_MANY_REQUESTS = '429.000';
    const CODE_SERVER_ERROR = '500.000';

	public $authenticated = FALSE;
	public $authorized = FALSE;

    /**
     * OS REST API constructor.
     *
     * @param boolean $handle_auth handle authentication and authorization
     *
     * The "handle_auth" parameter is for the "Discovery" controller
     * (/usr/clearos/framework/application/controllers/Discovery.php).
     * This Discovery controller will handle API discovery for requests, e.g.
     * https://192.168.1.100:83/api/v1 will show useful information on what
     * endpoints are available.  The authentication and authorization flags
     * are handled in this Discovery controller.
     *
     * For all other API requests, the authentication and authorization
     * are done right here.
     */

    public function __construct($handle_auth = TRUE)
    {
        parent::__construct();

        // Authentication
        //---------------

        // TODO
        //
        // Future: Digitally signed API requests (see Amazon for real-world example).
        // Now: a shared API key between the client and the OS REST API will do.

        $request_api_key = $this->input->get_request_header('x-api-key');
        $api_conf = file_get_contents('/etc/api.conf');

        if (!empty($api_conf)) {
            foreach (preg_split("/\n/", $api_conf) as $line) {
                $matches = array();
                if (preg_match('/^api_key\s*=\s*(.*)/', $line, $matches)) {
                    $localhost_api_key = $matches[1];
                    break;
                }
            }

            if ($request_api_key == $localhost_api_key)
                $this->authenticated = TRUE;
        }

        // Authorization
        //--------------

        // TODO
        //
        // For now, the shared API key provides access to everything.

        $this->authorized = TRUE;

        // Response
        //---------

        if (!$handle_auth)
            return;
        else if (!$this->authenticated)
            $this->respond_unauthorized();
        else if (!$this->authorized)
            $this->respond_forbidden();
    }

    /**
     * Standard exception handler for REST requests.
     *
     * @param exception $exception exception
     *
     * @return standard API response
     */

    public function exception_handler($exception)
    {
        if (is_a($exception, 'clearos\apps\base\Unauthorized_Exception'))
            $this->respond_unauthorized($exception->getMessage());
        else if (is_a($exception, 'clearos\apps\base\Forbidden_Exception'))
            $this->respond_forbidden($exception->getMessage());
        else if (is_a($exception, 'clearos\apps\base\Not_Found_Exception'))
            $this->respond_not_found($exception->getMessage());
        else if (is_a($exception, 'clearos\apps\base\Already_Exists_Exception'))
            $this->respond_already_exists($exception->getMessage());
        else if (is_a($exception, 'clearos\apps\base\Validation_Exception'))
            $this->respond_validation_error($exception->getMessage(), $exception->getDetails());
        else if (is_a($exception, 'clearos\apps\base\Client_Request_Exception'))
            $this->respond_bad_request($exception->getMessage());
        else if (is_a($exception, 'clearos\apps\base\Too_Many_Requests_Exception'))
            $this->respond_too_many_requests($exception->getMessage());
        else
            $this->respond_server_error($exception->getMessage());
    }

    // See codeigniter-restserver/application/libraries/REST_Controller.php

    public function respond_already_exists($message = '')
    {
        $http_code = self::HTTP_CODE_ALREADY_EXISTS;

        $payload['status_code'] = self::CODE_ALREADY_EXISTS;
        $payload['status_message'] = empty($message) ? 'Item already exists.' : $message;

        return parent::response($payload, $http_code, FALSE);
    }

    public function respond_bad_request($message = '')
    {
        $http_code = self::HTTP_CODE_BAD_REQUEST;

        $payload['status_code'] = self::CODE_BAD_REQUEST;
        $payload['status_message'] = empty($message) ? 'Invalid request.' : $message;

        return parent::response($payload, $http_code, FALSE);
    }

    public function respond_server_error($message = '')
    {
        $http_code = self::HTTP_CODE_SERVER_ERROR;

        $payload['status_code'] = self::CODE_SERVER_ERROR;
        $payload['status_message'] = empty($message) ? 'Server error.' : $message;

        return parent::response($payload, $http_code, FALSE);
    }

    public function respond_forbidden($message = '')
    {
        $http_code = self::HTTP_CODE_FORBIDDEN;

        $payload['status_code'] = self::CODE_FORBIDDEN;
        $payload['status_message'] = empty($message) ? 'Access is forbidden.' : $message;

        return parent::response($payload, $http_code, FALSE);
    }

    public function respond_not_found($message = '')
    {
        $http_code = self::HTTP_CODE_NOT_FOUND;

        $payload['status_code'] = self::CODE_NOT_FOUND;
        $payload['status_message'] = empty($message) ? 'Item not found.' : $message;

        return parent::response($payload, $http_code, FALSE);
    }

    public function respond_success($data = [], $options = [])
    {
        $http_code = self::HTTP_CODE_OK;

        $payload['status_code'] = self::CODE_OK;
        $payload['status_message'] = isset($options['message']) ? $options['message'] : 'Success';

        if (!empty($options['data_options']))
            $payload['data_options'] = $options['data_options'];

        if (!empty($data))
            $payload['data'] = $data;

        return parent::response($payload, $http_code, FALSE);
    }

    public function respond_too_many_requests($message = '')
    {
        $http_code = self::HTTP_CODE_TOO_MANY_REQUESTS;

        $payload['status_code'] = self::CODE_TOO_MANY_REQUESTS;
        $payload['status_message'] = empty($message) ? 'Too many requests.' : $message;

        return parent::response($payload, $http_code, FALSE);
    }

    public function respond_unauthorized($message = '')
    {
        $http_code = self::HTTP_CODE_UNAUTHORIZED;

        $payload['status_code'] = self::CODE_UNAUTHORIZED;
        $payload['status_message'] = empty($message) ? 'Unauthorized access.' : $message;

        return parent::response($payload, $http_code, FALSE);
    }

    public function respond_validation_error($message = '', $details = [])
    {
        $http_code = self::HTTP_CODE_VALIDATION_ERROR;

        $payload['status_code'] = self::CODE_VALIDATION_ERROR;
        $payload['status_message'] = empty($message) ? 'Validation error' : $message;

        if (!empty($details))
            $payload['validation_details'] = $details;

        return parent::response($payload, $http_code, FALSE);
    }
}

class ClearOS_REST_Controller extends OS_REST_Controller {}
class ClearOS_Controller extends OS_Controller {}
